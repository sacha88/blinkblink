# blinkblink

Little project to simulate a random blinking leds.

The circuit is based on the cd4060 IC.

## schematics kicad

The software used was kicad 7.1.

The Layout is optimized for home made PCB.

## IC info

https://components101.com/ics/cd4060-14-stage-binary-counter-ic

https://www.ti.com/lit/ds/symlink/cd4060b.pdf?ts=1681563452827&ref_url=https%253A%252F%252Fwww.google.pl%252F

## Inspirations

### VO
https://www.electroschematics.com/led-christmas-lights-circuit/

### other possibillities
https://www.circuits-diy.com/multiple-timing-led-flasher-using-cd4060-ic/

https://electronics.stackexchange.com/questions/164449/confusion-over-a-simple-4060-led-blinker


